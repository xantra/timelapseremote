package fr.penard.timelapseremote;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class Timelapse extends Fragment implements OnCheckedChangeListener, View.OnClickListener, MainActivity.JsonPacketReceived {

    private View view;

    private Long numberOfFrame = 0L;
    private Long delayH = 0L;
    private Long delayM = 0L;
    private Long delayS = 0L;
    private Boolean movingTimelapse = true;
    private Boolean moveRight = true;
    private Boolean stopForShot = true;
    private Boolean selectSteps = true;
    private Long stepsPerShot = 0L;
    private Long angleInDegree = 0L;


    public Timelapse() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (view == null) {

            // Inflate the layout for this fragment
            view = inflater.inflate(R.layout.fragment_timelapse, container, false);

            // Listener for the send button
            view.findViewById(R.id.send_timelapse).setOnClickListener(this);

            // Set listener for textView and checkBox
            initTextChange();
            ((CheckBox) view.findViewById(R.id.moving_timelapse)).setOnCheckedChangeListener(this);
            ((RadioButton) view.findViewById(R.id.moveRight)).setOnCheckedChangeListener(this);
            ((CheckBox) view.findViewById(R.id.stop_for_shot)).setOnCheckedChangeListener(this);
            ((RadioButton) view.findViewById(R.id.selectSteps)).setOnCheckedChangeListener(this);

        }


        return view;
    }

    public void onResume (){
        super.onResume();

        Log.d("onResume", "");
        refreshView();
    }


    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        Log.d("onCheckedChanged", "");

        if (buttonView.getId() == R.id.moving_timelapse) {
            final View view = buttonView.getRootView().findViewById(R.id.moving_timelapse_view);

            if (getActivity().getCurrentFocus() != null) {

                Log.d("timelapse", "clear focus");
                getActivity().getCurrentFocus().clearFocus();
                InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }

            view.setVisibility(isChecked ? View.VISIBLE : View.GONE);
            movingTimelapse = isChecked;

        } else if (buttonView.getId() == R.id.stop_for_shot) {
            stopForShot = isChecked;
        } else if (buttonView.getId() == R.id.moveRight) {
            moveRight = isChecked;
        } else if (buttonView.getId() == R.id.selectSteps) {
            selectSteps = isChecked;
            view.findViewById(R.id.angle).setEnabled(!isChecked);
            view.findViewById(R.id.steps_per_shot).setEnabled(isChecked);
        }
    }

    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.send_timelapse) {

            Option<Long> nb_shots;
            Option<Long> intershot_delay;
            Option<Long> steps_per_shot;
            Option<Boolean> move_right;
            Option<Boolean> stop_for_shot;

            Long delayInS = delayH * 3600 + delayM * 60 + delayS;

            // Values to send
            nb_shots = new Option<>(numberOfFrame);
            intershot_delay = new Option<>(delayInS);
            if (movingTimelapse) {
                steps_per_shot = new Option<>(stepsPerShot);
                move_right = new Option<>(moveRight);
                stop_for_shot = new Option<>(stopForShot);
            } else {
                steps_per_shot = new Option<>();
                move_right = new Option<>();
                stop_for_shot = new Option<>();
            }

            JSONObject jsonRequest;
            try {
                jsonRequest = JsonProtocol.generateTimelapsePacket(nb_shots, intershot_delay, steps_per_shot, move_right, stop_for_shot);
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }

            Log.d("sendJson", jsonRequest.toString());
            ((MainActivity)getActivity()).bluetoothAdapter.send(jsonRequest);
        }
    }

    @Override
    public void onReceiveJsonPacket(JSONObject jsonObject) {
        Log.d("onReceiveJsonPacket", "");

        try {
            numberOfFrame = jsonObject.getLong(JsonProtocol.NB_SHOTS);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            Long delayInS = jsonObject.getLong(JsonProtocol.INTERSHOT_DELAY);
                    // delayInS = delayH * 3600 + delayM * 60 + delayS;

            delayH = delayInS / 3600;
            delayInS = delayInS % 3600;

            delayM = delayInS / 60;
            delayInS = delayInS % 60;

            delayS = delayInS;

        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            JSONObject panning = jsonObject.getJSONObject(JsonProtocol.PANNING);
            movingTimelapse = true;

            try {
                stepsPerShot = panning.getLong(JsonProtocol.STEPS_PER_SHOT);
                selectSteps = true;

            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                moveRight = panning.getBoolean(JsonProtocol.MOVE_RIGHT);
            } catch (JSONException e) {
                e.printStackTrace();
            }

            try {
                stopForShot = panning.getBoolean(JsonProtocol.STOP_FOR_SHOT);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (JSONException e) {
            movingTimelapse = false;
        }

        if (view != null) {
            refreshView();
        }
    }

    //
    // OnChange management
    //

    private void initTextChange() {
        Log.d("initTextChange", "");
        ((TextView)view.findViewById(R.id.number_of_frame)).addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() != 0)
                            numberOfFrame = Long.valueOf(s.toString());
                        else
                            numberOfFrame = 0L;
                        processAngle();
                        processStepsPerShot();
                    }
                }
        );
        ((TextView)view.findViewById(R.id.delay_h)).addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() != 0)
                            delayH = Long.valueOf(s.toString());
                        else
                            delayH = 0L;
                    }
                }
        );
        ((TextView)view.findViewById(R.id.delay_m)).addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() != 0)
                            delayM = Long.valueOf(s.toString());
                        else
                            delayM = 0L;
                    }
                }
        );
        ((TextView)view.findViewById(R.id.delay_s)).addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() != 0)
                            delayS = Long.valueOf(s.toString());
                        else
                            delayS = 0L;
                    }
                }
        );
        ((TextView)view.findViewById(R.id.steps_per_shot)).addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() != 0)
                            stepsPerShot = Long.valueOf(s.toString());
                        else
                            stepsPerShot = 0L;
                        processStepsPerShot();
                    }
                }
        );
        ((TextView)view.findViewById(R.id.angle)).addTextChangedListener(
                new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        if (s.length() != 0)
                            angleInDegree = Long.valueOf(s.toString());
                        else
                            angleInDegree = 0L;
                        processAngle();
                    }
                }
        );
    }


    void processStepsPerShot() {
        Log.d("processStepsPerShot", "");
        Log.d("processStepsPerShot", ((MainActivity) getActivity()).settings_fragment.getNbStepsForFullPanning().toString());
        if (selectSteps) {
            if (((MainActivity) getActivity()).settings_fragment.getNbStepsForFullPanning() != 0L
                    && stepsPerShot != 0L
                    && numberOfFrame >= 2) {

                angleInDegree = stepsPerShot * (numberOfFrame - 1) * 360
                        / ((MainActivity) getActivity()).settings_fragment.getNbStepsForFullPanning();
            } else {
                angleInDegree = 0L;
            }
            Log.d("processStepsPerShot", "set angle " + angleInDegree.toString());

            if (angleInDegree == 0L)
                ((TextView)view.findViewById(R.id.angle)).setText("");
            else
                ((TextView)view.findViewById(R.id.angle)).setText(angleInDegree.toString());
        }
    }


    void processAngle() {
        Log.d("processAngle", "");
        if (!selectSteps) {
            if (((MainActivity) getActivity()).settings_fragment.getNbStepsForFullPanning() != 0L
                    && angleInDegree != 0L
                    && numberOfFrame >= 2) {

                stepsPerShot = angleInDegree * ((MainActivity) getActivity()).settings_fragment.getNbStepsForFullPanning()
                        / (360 * (numberOfFrame - 1));
            } else {
                stepsPerShot = 0L;
            }
            if (stepsPerShot == 0L)
                ((TextView)view.findViewById(R.id.steps_per_shot)).setText("");
            else
                ((TextView)view.findViewById(R.id.steps_per_shot)).setText(stepsPerShot.toString());
        }
    }

    void refreshView() {
        Log.d("refreshView", "");

        if (numberOfFrame == 0L)
            ((TextView)view.findViewById(R.id.number_of_frame)).setText("");
        else
            ((TextView)view.findViewById(R.id.number_of_frame)).setText(numberOfFrame.toString());

        if (delayH == 0L)
            ((TextView)view.findViewById(R.id.delay_h)).setText("");
        else
            ((TextView)view.findViewById(R.id.delay_h)).setText(delayH.toString());

        if (delayM == 0L)
            ((TextView)view.findViewById(R.id.delay_m)).setText("");
        else
            ((TextView)view.findViewById(R.id.delay_m)).setText(delayM.toString());

        if (delayS == 0L)
            ((TextView)view.findViewById(R.id.delay_s)).setText("");
        else
            ((TextView)view.findViewById(R.id.delay_s)).setText(delayS.toString());

        ((CheckBox)view.findViewById(R.id.moving_timelapse)).setChecked(movingTimelapse);
        ((RadioButton)view.findViewById(R.id.moveRight)).setChecked(moveRight);
        ((RadioButton)view.findViewById(R.id.moveLeft)).setChecked(!moveRight);
        ((CheckBox)view.findViewById(R.id.stop_for_shot)).setChecked(stopForShot);
        ((RadioButton)view.findViewById(R.id.selectSteps)).setChecked(selectSteps);
        ((RadioButton)view.findViewById(R.id.selectAngle)).setChecked(!selectSteps);

        if (stepsPerShot == 0L)
            ((TextView)view.findViewById(R.id.steps_per_shot)).setText("");
        else
            ((TextView)view.findViewById(R.id.steps_per_shot)).setText(stepsPerShot.toString());

        Log.d("refreshView", "set angle " + angleInDegree.toString());
        if (angleInDegree == 0L)
            ((TextView)view.findViewById(R.id.angle)).setText("");
        else
            ((TextView)view.findViewById(R.id.angle)).setText(angleInDegree.toString());
    }
}
