package fr.penard.timelapseremote;

import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * Created by jordan on 13/06/15.
 */
public class DrawerAdapter extends RecyclerView.Adapter<ViewHolder> implements ViewHolder.ViewHolderCallback {


    private final RecyclerView recyclerView;
    private final FragmentCollection fragmentCollection;
    private final DrawerAdapterCallback mainActivityCallback;

    private int selectedItem = 0;


    DrawerAdapter(FragmentCollection fragmentCollection, MainActivity mainActivity, RecyclerView recyclerView) {
        this.fragmentCollection = fragmentCollection;
        this.recyclerView = recyclerView;

        mainActivityCallback = mainActivity;
    }


    @Override
    public void onItemClicked(int itemClicked) {
        mainActivityCallback.onItemClicked(itemClicked);

        makeViewUnselected(recyclerView.getChildAt(selectedItem));
        selectedItem = itemClicked;
        makeViewSelected(recyclerView.getChildAt(selectedItem));
    }


    //Below first we override the method onCreateViewHolder which is called when the ViewHolder is
    //Created, In this method we inflate the item_row.xml layout if the viewType is Type_ITEM or else we inflate header.xml
    // if the viewType is TYPE_HEADER
    // and pass it to the view holder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int i) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.navigation_item_row,parent,false); //Inflating the layout

        return new ViewHolder(v, this); //Creating ViewHolder and passing the object of type view
    }


    //Next we override a method which is called when the item in a row is needed to be displayed, here the int position
    // Tells us item at which position is being constructed to be displayed and the holder id of the holder object tell us
    // which view type is being created 1 for item row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // position by 1 and pass it to the holder while setting the text and image
        holder.textView.setText(fragmentCollection.getFragmentName(position)); // Setting the Text with the array of our Titles
        holder.imageView.setImageResource(fragmentCollection.getFragmentIcon(position));// Setting the image with array of our icons

        if (position == selectedItem)
            makeViewSelected(holder.itemView);
        else
            makeViewUnselected(holder.itemView);
    }

    // This method returns the number of items present in the list
    @Override
    public int getItemCount() {
        return fragmentCollection.getFragmentCount();
    }


    private void makeViewSelected(View view) {
        view.setBackgroundResource(R.drawable.drawer_item_selected);
    }

    private void makeViewUnselected(View view) {
        view.setBackgroundResource(R.drawable.drawer_item_unselected);
    }


    public interface DrawerAdapterCallback{
        void onItemClicked (int itemClicked);
    }
}
