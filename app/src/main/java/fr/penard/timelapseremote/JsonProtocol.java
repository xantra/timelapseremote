package fr.penard.timelapseremote;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * The class will manage the encoding and decoding of packets exchanged with the Arduino board
 * This is based on the JSON_spec.txt v2.1
 */
public final class JsonProtocol {


    public static final String CURRENT_PROTOCOL_VERSION = "v2.1";

    public static final String TIMELAPSE_SETUP = "timelapse_setup";
    public static final String NB_SHOTS = "nb_shots";
    public static final String INTERSHOT_DELAY = "intershot_delay";
    public static final String PANNING = "panning";
    public static final String STEPS_PER_SHOT = "steps_per_shot";
    public static final String MOVE_RIGHT = "move_right";
    public static final String STOP_FOR_SHOT = "stop_for_shot";
    public static final String BOARD_SETTINGS = "board_settings";
    public static final String PROTOCOL_VERSION = "protocol_version";
    public static final String STRICT_MODE = "strict_mode";
    public static final String CAPABILITIES = "capabilities";
    public static final String ONESHOT_SHUTTER_HOLD_DELAY = "oneshot_shutter_hold_delay";
    public static final String NB_STEPS_FOR_FULL_PANNING = "nb_steps_for_full_panning";
    public static final String REQUEST = "request";
    public static final String REQUEST_ALL = "all";
    public static final String REQUEST_ACTIVES = "actives";


    enum requests {REQUEST_ALL, REQUEST_ACTIVES, REQUEST_BOARD_SETTINGS, REQUEST_TIMELAPSE_SETUP};

    static JSONObject generateTimelapsePacket(Option<Long> nb_shots,
                             Option<Long> intershot_delay,
                             Option<Long> steps_per_shot,
                             Option<Boolean> move_right,
                             Option<Boolean> stop_for_shot) throws JSONException {

        try {

            if (nb_shots.asValue() || intershot_delay.asValue() || steps_per_shot.asValue() || move_right.asValue() || stop_for_shot.asValue()) {

                JSONObject timelapse_packet = new JSONObject();

                addItemIfNeeded(timelapse_packet, NB_SHOTS, nb_shots);
                addItemIfNeeded(timelapse_packet, INTERSHOT_DELAY, intershot_delay);

                if (steps_per_shot.asValue() || move_right.asValue() || stop_for_shot.asValue()) {
                    JSONObject panning_packet = new JSONObject();

                    addItemIfNeeded(panning_packet, STEPS_PER_SHOT, steps_per_shot);
                    addItemIfNeeded(panning_packet, MOVE_RIGHT, move_right);
                    addItemIfNeeded(panning_packet, STOP_FOR_SHOT, stop_for_shot);

                    timelapse_packet.put(PANNING, panning_packet);
                }

                return new JSONObject().put(TIMELAPSE_SETUP, timelapse_packet);
            } else {
                return new JSONObject();
            }
        }
        catch (JSONException e) {
            throw e;
        }

    }

    private static void addItemIfNeeded(JSONObject jsonObject, String key, Option value) {
        try {
            jsonObject.put(key, value.getValue());
        }
        catch (NoValueException e) {} // Nothing to do, we don't want to add that field
        catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public static JSONObject generateSettingsPacket(Option<Boolean> strict_mode,
                                                    Option<Long> oneshot_shutter_hold_delay,
                                                    Option<Long> nb_steps_for_full_panning) throws JSONException {

        try {

            if (strict_mode.asValue() || oneshot_shutter_hold_delay.asValue() || nb_steps_for_full_panning.asValue()) {

                JSONObject settings_packet = new JSONObject();

                settings_packet.put(PROTOCOL_VERSION, CURRENT_PROTOCOL_VERSION);
                addItemIfNeeded(settings_packet, STRICT_MODE, strict_mode);
                addItemIfNeeded(settings_packet, ONESHOT_SHUTTER_HOLD_DELAY, oneshot_shutter_hold_delay);
                addItemIfNeeded(settings_packet, NB_STEPS_FOR_FULL_PANNING, nb_steps_for_full_panning);

                return new JSONObject().put(BOARD_SETTINGS, settings_packet);
            } else {
                return new JSONObject();
            }
        }
        catch (JSONException e) {
            throw e;
        }
    }

    public static JSONObject generateRequestPacket(requests request) throws JSONException {

        try {

            String requestToSend;

            switch (request) {
                case REQUEST_ALL:
                    requestToSend = REQUEST_ALL;
                    break;

                case REQUEST_ACTIVES:
                    requestToSend = REQUEST_ACTIVES;
                    break;

                case REQUEST_BOARD_SETTINGS:
                    requestToSend = BOARD_SETTINGS;
                    break;

                case REQUEST_TIMELAPSE_SETUP:
                    requestToSend = TIMELAPSE_SETUP;
                    break;

                default:
                    throw new RuntimeException("case item missing in generateRequestPacket");
            }

            return new JSONObject().put(REQUEST, requestToSend);
        }
        catch (JSONException e) {
            throw e;
        }
    }
}

