package fr.penard.timelapseremote;


import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * A simple {@link Fragment} subclass.
 */
public class Settings extends Fragment implements View.OnClickListener, MainActivity.JsonPacketReceived, CompoundButton.OnCheckedChangeListener {


    View view;

    private String boardProtocolVersion = "x.x";
    private Boolean strictMode = false;
    private Integer boardCapabilities = 0;
    private Long oneshotShutterHoldDelay = 0L;
    private Long nbStepsForFullPanning = 0L;


    public Settings() {
        // Required empty public constructor
    }


    Long getNbStepsForFullPanning() {
        return nbStepsForFullPanning;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_settings, container, false);

        // Listener for the send button
        view.findViewById(R.id.send_settings).setOnClickListener(this);

        // Set App version code
        ((TextView)view.findViewById(R.id.appProtocolVersion)).setText(JsonProtocol.CURRENT_PROTOCOL_VERSION);

        // Set listener for textView and checkBox
        initTextChange();
        ((CheckBox)view.findViewById(R.id.strict_mode)).setOnCheckedChangeListener(this);

        refreshView();

        return view;
    }


    @Override
    public void onClick(View v) {

        if (v.getId() == R.id.send_settings) {

            // Values to send
            Option<Boolean> strict_mode = new Option<>(strictMode);
            Option<Long> oneshot_shutter_hold_delay = new Option<>(oneshotShutterHoldDelay);
            Option<Long> nb_steps_for_full_panning = new Option<>(nbStepsForFullPanning);

            JSONObject jsonRequest;
            try {
                jsonRequest = JsonProtocol.generateSettingsPacket(strict_mode, oneshot_shutter_hold_delay, nb_steps_for_full_panning);
            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }

            //Log.d("sendJson", jsonRequest.toString());
            ((MainActivity)getActivity()).bluetoothAdapter.send(jsonRequest);
        }
    }

    @Override
    public void onReceiveJsonPacket(JSONObject jsonObject) {

        try {
            boardProtocolVersion = jsonObject.getString(JsonProtocol.PROTOCOL_VERSION);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            strictMode = jsonObject.getBoolean(JsonProtocol.STRICT_MODE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            boardCapabilities = jsonObject.getInt(JsonProtocol.CAPABILITIES);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            oneshotShutterHoldDelay = jsonObject.getLong(JsonProtocol.ONESHOT_SHUTTER_HOLD_DELAY);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        try {
            nbStepsForFullPanning = jsonObject.getLong(JsonProtocol.NB_STEPS_FOR_FULL_PANNING);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        if (view != null) {
            refreshView();
        }
    }

    void refreshView() {
        ((TextView)view.findViewById(R.id.boardProtocolVersion)).setText(boardProtocolVersion);
        ((CheckBox)view.findViewById(R.id.strict_mode)).setChecked(strictMode);
        ((TextView)view.findViewById(R.id.boardCapabilities)).setText(String.valueOf(boardCapabilities));
        ((TextView)view.findViewById(R.id.oneshot_shutter_hold_delay)).setText(String.valueOf(oneshotShutterHoldDelay));
        ((TextView)view.findViewById(R.id.nb_steps_for_full_panning)).setText(String.valueOf(nbStepsForFullPanning));
    }



    //
    // OnChange management
    //

    private void initTextChange() {
        ((TextView)view.findViewById(R.id.oneshot_shutter_hold_delay)).addTextChangedListener(
            new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() != 0)
                        oneshotShutterHoldDelay = Long.valueOf(s.toString());
                    else
                        oneshotShutterHoldDelay = 0L;
                }
            }
        );
        ((TextView)view.findViewById(R.id.nb_steps_for_full_panning)).addTextChangedListener(
            new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    if (s.length() != 0)
                        nbStepsForFullPanning = Long.valueOf(s.toString());
                    else
                        nbStepsForFullPanning = 0L;
                }
            }
        );
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (buttonView.getId() == R.id.strict_mode) {
            strictMode = isChecked;
        }
    }
}
