package fr.penard.timelapseremote;

// Exception thrown when getValue is called on an Option when there is no available value in this Option
public class NoValueException extends Exception {
    NoValueException(String msg) {
        super(msg);
    }
}
