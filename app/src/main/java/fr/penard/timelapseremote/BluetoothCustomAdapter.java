package fr.penard.timelapseremote;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Set;
import java.util.UUID;

/**
 * This class is managing the Bluetooth (Connection and Communication with the Arduino board)
 */
public class BluetoothCustomAdapter {

    private final MainActivity mainActivity;
    private final android.bluetooth.BluetoothAdapter mBluetoothAdapter = android.bluetooth.BluetoothAdapter.getDefaultAdapter();

    private BluetoothSocket BTSocket;
    private BluetoothDevice BTDevice;
    private InputStream inputStream = null;
    private OutputStream outputStream = null;
    private ProgressDialog progressDialog;

    private Thread commThread;


    public BluetoothCustomAdapter(MainActivity mainActivity) throws Exception {
        this.mainActivity = mainActivity;

        BTDevice = null;
        BTSocket = null;
        inputStream = null;
        outputStream = null;
        commThread = null;

        // Test if Bluetooth is supported
        if (mBluetoothAdapter == null) {
            // Device does not support Bluetooth
            throw new Exception("Device does not support Bluetooth");
        }

        // Check if Bluetooth is enabled
        // If not, ask to enable it
        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(android.bluetooth.BluetoothAdapter.ACTION_REQUEST_ENABLE);
            mainActivity.startActivityForResult(enableBtIntent, 0);
        }
    }

    public void connect() {

        // List paired Bluetooth devices
        final ArrayList<String> devicesNames = new ArrayList<>();
        final ArrayList<BluetoothDevice> devices = new ArrayList<>();

        final Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                devicesNames.add(device.getName());
                devices.add(device);
            }
        }

        // Ask the user to pick a device
        AlertDialog.Builder builder = new AlertDialog.Builder(mainActivity);
        builder.setTitle("Select a timelapse device");
        builder.setItems(devicesNames.toArray(new CharSequence[devicesNames.size()]), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                connect(devices.get(item)); // Connect to the selected device
            }
        });
        builder.create().show();
    }

    // Will cancel an in-progress connection, and close the socket
    public void disconnect() {
        if (commThread != null && commThread.isAlive()){
            // TODO We need to stop it....
        }
        if (BTSocket != null) {
            try {
                BTSocket.close();
            } catch (IOException e) {
            }
        }
    }

    // Connect to the selected device and run the comm. thread
    private void connect(BluetoothDevice device) {
        disconnect();

        BTDevice = device;
        progressDialog = ProgressDialog.show(mainActivity, "Connecting", "Please wait...", true);

        commThread = new Thread(runnable);
        commThread.start();
    }

    // The thread managing the connection and the communication
    Runnable runnable = new Runnable() {
        public void run() {

            // Cancel discovery because it will slow down the connection
            mBluetoothAdapter.cancelDiscovery();

            // Get a BluetoothSocket to connect with the given BluetoothDevice
            try {
                // MY_UUID is the app's UUID string, also used by the server code
                BTSocket = BTDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
            } catch (IOException e) {
                Log.e("BluetoothAdapter", e.toString());
                progressDialog.dismiss();
                errorHandler.sendEmptyMessage(0);
                return;
            }

            // Connect the device through the socket. This will block
            // until it succeeds or throws an exception
            try {
                BTSocket.connect();
            } catch (IOException connectException) {
                // Unable to connect; close the socket and get out
                try {
                    BTSocket.close();
                } catch (IOException closeException) {
                }
                Log.e("BluetoothAdapter", "Can't connect to the Bluetooth device");
                Log.e("BluetoothAdapter", connectException.toString());
                progressDialog.dismiss();
                errorHandler.sendEmptyMessage(0);
                return;
            }

            // Get the input and output streams
            try {
                inputStream = BTSocket.getInputStream();
                outputStream = BTSocket.getOutputStream();
            } catch (IOException e) {
            }

            try {
                send(JsonProtocol.generateRequestPacket(JsonProtocol.requests.REQUEST_ALL));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            progressDialog.dismiss();

            byte[] buffer = new byte[1024];
            int begin = 0;
            int bytes = 0;
            while (true) {
                try {
                    bytes += inputStream.read(buffer, bytes, buffer.length - bytes);
                    for(int i = begin; i < bytes; i++) {
                        if (buffer[i] == 0) {
                            //Log.d("BTReceive", bytes + " bytes");
                            mHandler.obtainMessage(1, begin, i-1, buffer).sendToTarget();
                            begin = i+1;
                            if(i == bytes - 1) {
                                bytes = 0;
                                begin = 0;
                                //Log.d("BTReceive", "Buffer reset");
                            }
                        }
                    }
                } catch (IOException e) {
                    break;
                }
            }
        }
    };

    Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            byte[] writeBuf = (byte[]) msg.obj;
            int begin = (int)msg.arg1;
            int end = (int)msg.arg2;

            switch(msg.what) {
                case 1:
                    String writeMessage = new String(writeBuf);
                    //Log.d("BTReceive", "begin : " + begin + ", end : " + (end+1));
                    Log.d("BTReceive", writeMessage.substring(begin, end+1));
                    mainActivity.onReceiveJsonPacket(writeMessage.substring(begin, end+1));
                    break;
            }
        }
    };

    // Error feedback handler for the comm thread
    Handler errorHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            new AlertDialog.Builder(mainActivity)
                    .setTitle("Error durring connexion")
                    .setMessage("Can't connect to Bluetooth device")
                    .setNeutralButton("Close", null)
                    .show();
        }
    };

    // Call this from the main activity to send data to the remote device
    public void send(JSONObject request) {
        if (BTSocket != null && BTSocket.isConnected() && outputStream != null) {
            try {
                Log.d("BTSend", "sending...");
                Log.d("BTSend", request.toString());
                outputStream.write((request.toString() + '\0').getBytes());
                Log.d("BTSend", "Done");
            } catch (IOException e) {
                Log.e("BTSend", e.toString());
            }
        }
    }


    public Boolean isConnected() {
        if (BTSocket != null)
            return BTSocket.isConnected();
        else
            return false;
    }

}

