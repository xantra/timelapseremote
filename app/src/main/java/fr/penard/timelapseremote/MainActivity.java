package fr.penard.timelapseremote;

import android.content.Context;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.support.v4.widget.DrawerLayout;

import android.support.v7.widget.Toolbar;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Iterator;

public class MainActivity extends AppCompatActivity implements DrawerAdapter.DrawerAdapterCallback {


    private Toolbar toolbar;

    RecyclerView mRecyclerView;                           // Declaring RecyclerView
    RecyclerView.Adapter mAdapter;                        // Declaring Adapter For Recycler View
    RecyclerView.LayoutManager mLayoutManager;            // Declaring Layout Manager as a linear layout manager
    DrawerLayout Drawer;                                  // Declaring DrawerLayout

    ActionBarDrawerToggle mDrawerToggle;                  // Declaring Action Bar Drawer Toggle

    BluetoothCustomAdapter bluetoothAdapter;

    FragmentCollection fragmentCollection;

    Bundle savedInstanceState;

    boolean autoRefreshEnable = false;

    Settings settings_fragment;
    Timelapse timelapse_fragment;

    private int mInterval = 5000; // 5 seconds by default, can be changed later
    private Handler mHandler;



    @Override
    protected void onDestroy() {
        super.onDestroy();
        bluetoothAdapter.disconnect();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.savedInstanceState = savedInstanceState;
        setContentView(R.layout.activity_main);

        settings_fragment = new Settings();
        timelapse_fragment = new Timelapse();

        fragmentCollection = new FragmentCollection();
        fragmentCollection.addFragment(getString(R.string.title_timelapse), R.drawable.mode_timelapse, timelapse_fragment);
        fragmentCollection.addFragment(getString(R.string.title_settings), R.drawable.mode_setting, settings_fragment);

        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar); // Attaching the layout to the toolbar object

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view); // Assigning the RecyclerView Object to the xml View
        mRecyclerView.setHasFixedSize(true);                            // Letting the system know that the list objects are of fixed size

        mAdapter = new DrawerAdapter(fragmentCollection, this, mRecyclerView);       // Creating the Adapter of MyAdapter class(which we are going to see in a bit)
        mRecyclerView.setAdapter(mAdapter);                              // Setting the adapter to RecyclerView

        mLayoutManager = new LinearLayoutManager(this);                 // Creating a layout Manager
        mRecyclerView.setLayoutManager(mLayoutManager);                 // Setting the layout Manager

        Drawer = (DrawerLayout) findViewById(R.id.drawer_layout);        // Drawer object Assigned to the view
        mDrawerToggle = new ActionBarDrawerToggle(this,Drawer,toolbar,R.string.navigation_drawer_open,R.string.navigation_drawer_close){

            // Clear focus and close the keyboard when the drawer move
            public void onDrawerStateChanged (int newState) {
                if (newState == DrawerLayout.STATE_SETTLING){
                    View current = getCurrentFocus();
                    if (current != null) {
                        current.clearFocus();
                        InputMethodManager imm = (InputMethodManager)getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(current.getWindowToken(), 0);
                    }
                }
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                // code here will execute once the drawer is opened( As I dont want anything happened whe drawer is
                // open I am not going to put anything here)
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                // Code here will execute once drawer is closed
            }
        }; // Drawer Toggle Object Made

        Drawer.setDrawerListener(mDrawerToggle); // Drawer Listener set to the Drawer toggle
        mDrawerToggle.syncState();               // Finally we set the drawer toggle sync State

        onItemClicked(0);

        mHandler = new Handler();

        // Instantiate the custom bluetooth adapter
        try {
            bluetoothAdapter = new BluetoothCustomAdapter(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        // Ask the user to connect a bluetooth device
        bluetoothAdapter.connect();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                bluetoothAdapter.send(JsonProtocol.generateRequestPacket(JsonProtocol.requests.REQUEST_ACTIVES));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mHandler.postDelayed(mStatusChecker, mInterval);
        }
    };


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    private void enableAutoRefresh() {
        // Change the refresh logo
        toolbar.getMenu().findItem(R.id.item_refresh).setIcon(R.drawable.refresh_color_accent);

        // Run the repeated task
        mStatusChecker.run();

        autoRefreshEnable = true;
    }

    private void disableAutoRefresh() {
        // Change the refresh logo
        toolbar.getMenu().findItem(R.id.item_refresh).setIcon(R.drawable.refresh_white);

        // Stop the repeated task
        mHandler.removeCallbacks(mStatusChecker);

        autoRefreshEnable = false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.item_refresh && bluetoothAdapter.isConnected()) {
            if (autoRefreshEnable) {
                disableAutoRefresh();
            } else {
                enableAutoRefresh();
            }
            return true;

        } else if (id == R.id.connect_to_device) {
            bluetoothAdapter.connect();
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onItemClicked(int itemClicked) {
        getFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, fragmentCollection.getFragment(itemClicked))
                .commit();

        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null)
            toolbar.setTitle(fragmentCollection.getFragmentName(itemClicked));
        else
            actionBar.setTitle(fragmentCollection.getFragmentName(itemClicked));

        Drawer.closeDrawers();
    }

    void onReceiveJsonPacket(String jsonPacket) {

        JSONObject jsonObject;

        try {
            jsonObject = new JSONObject(jsonPacket);
        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        for (Iterator<String> items = jsonObject.keys(); items.hasNext();){
            String currentKey = items.next();

            try {
                switch (currentKey) {
                    case "board_settings":
                        settings_fragment.onReceiveJsonPacket(jsonObject.getJSONObject(currentKey));
                        break;

                    case "timelapse_setup":
                        timelapse_fragment.onReceiveJsonPacket(jsonObject.getJSONObject(currentKey));
                        break;

                    case "ack":
                        switch (jsonObject.getJSONObject(currentKey).getInt("id")) {
                            case 0:
                                if (autoRefreshEnable) {
                                    disableAutoRefresh();
                                }

                                Log.d("ack", "OK");

                                Context context = getApplicationContext();
                                CharSequence text = "OK";
                                int duration = Toast.LENGTH_SHORT;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();

                                break;

                            default:
                                // TODO
                                break;
                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public interface JsonPacketReceived{
        void onReceiveJsonPacket(JSONObject jsonObject);
    }
}
