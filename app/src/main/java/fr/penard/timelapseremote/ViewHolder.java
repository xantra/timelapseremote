package fr.penard.timelapseremote;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by jordan on 14/06/15.
 */

// Creating a ViewHolder which extends the RecyclerView View Holder
// ViewHolder are used to to store the inflated views in order to recycle them
public class ViewHolder extends RecyclerView.ViewHolder implements RecyclerView.OnClickListener {


    TextView textView;
    ImageView imageView;
    View itemView;

    ViewHolderCallback callback;


    public ViewHolder(View itemView, ViewHolderCallback callback) {                 // Creating ViewHolder Constructor with View and viewType As a parameter
        super(itemView);
        itemView.setClickable(true);
        itemView.setOnClickListener(this);

        this.itemView = itemView;
        this.callback = callback;

        textView = (TextView) itemView.findViewById(R.id.rowText); // Creating TextView object with the id of textView from item_row.xml
        imageView = (ImageView) itemView.findViewById(R.id.rowIcon);// Creating ImageView object with the id of ImageView from item_row.xml

    }

    @Override
    public void onClick(View v) {
        callback.onItemClicked(getAdapterPosition());
    }

    public interface ViewHolderCallback{
        void onItemClicked (int itemClicked);
    }

}


