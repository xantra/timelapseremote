package fr.penard.timelapseremote;

public class Option<T> {

    private final T value;
    private final boolean asValue;

    Option() {
        value = null;
        asValue = false;
    }

    Option(T value) {
        this.value = value;
        asValue = true;
    }

    boolean asValue() {
        return asValue;
    }

    T getValue() throws NoValueException {
        if (!asValue)
            throw new NoValueException("There is no value in this Option");
        else
            return value;
    }

}

