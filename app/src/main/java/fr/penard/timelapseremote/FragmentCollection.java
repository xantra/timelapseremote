package fr.penard.timelapseremote;

import android.app.Fragment;

import java.util.ArrayList;

/**
 * Created by jordan on 13/06/15.
 */
public class FragmentCollection {

    ArrayList<Fragment> fragments;
    ArrayList<String> fragmentsName;
    ArrayList<Integer> fragmentsIcon;

    FragmentCollection() {
        fragments = new ArrayList<>();
        fragmentsName = new ArrayList<>();
        fragmentsIcon = new ArrayList<>();
    }

    public void addFragment(String name, Integer iconResourceID, Fragment fragment) {
        fragments.add(fragment);
        fragmentsName.add(name);
        fragmentsIcon.add(iconResourceID);
    }

    public Fragment getFragment(int index) {
        return fragments.get(index);
    }

    public Integer getFragmentIcon(int index) {
        return fragmentsIcon.get(index);
    }

    public String getFragmentName(int index) {
        return fragmentsName.get(index);
    }

    public Integer getFragmentCount() {
        return fragments.size();
    }
}
